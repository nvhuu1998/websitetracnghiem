const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const validateSubject = require('../validation/subject');

//get all subject 
router.get('/', async (req, res) => {
  let subjects;
  try {
    subjects = await Subjects.find().sort({ name: 1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: subjects, total: subjects.length })
})
// get subject by page perPage
router.post('/', async (req, res) => {
  let subjects, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Subjects.find()
    subjects = await Subjects.find().sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: subjects, length: subjects.length, total: total.length })
})
//get subject by id
router.get('/:id', async (req, res) => {
  let subject, id = req.params.id;
  try {
    subject = await Subjects.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Không tìm được môn!' })
  }
  res.status(201).json({ data: subject })
})
// add subject
router.post('/add', async (req, res) => {
  const { errors, isValid } = validateSubject(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { name, creator } = req.body
  const subject = new Subjects({
    name,
    creator
  })
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  try {
    await subject.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Đã tạo môn học', data: subject })
})
// delete subject
router.delete('/:id', async (req, res) => {

  const id = req.params.id
  let subject
  try {
    subject = await Subjects.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Không tìm được môn!' })
  }
  try {
    await subject.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa môn thành công!' })
})
// update subject
router.patch('/:id', async (req, res) => {
  const id = req.params.id
  let subject
  try {
    subject = await Subjects.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Không tìm được môn!' })
  }
  const { name, creator } = req.body
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  subject.name = name;
  subject.creator = creator
  try {
    await subject.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Cập nhật môn học thành công', data: subject })
})
module.exports = router;