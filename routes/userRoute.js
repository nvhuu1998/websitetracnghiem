const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');

const User = require('../models/userModel');

router.post('/register', function (req, res) {

  const { errors, isValid } = validateRegisterInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({
    email: req.body.email
  }).then(user => {
    if (user) {
      return res.status(400).json({
        email: 'Email already exists'
      });
    }
    else {
      const avatar = gravatar.url(req.body.email, {
        s: '200',
        r: 'pg',
        d: 'mm'
      });
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        avatar,
        // phone: req.body.phone,
        role: req.body.role
      });

      bcrypt.genSalt(10, (err, salt) => {
        if (err) console.error('There was an error', err);
        else {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) console.error('There was an error', err);
            else {
              newUser.password = hash;
              newUser
                .save()
                .then(user => {
                  res.json(user)
                });
            }
          });
        }
      });
    }
  });
});

router.post('/login', (req, res) => {

  const { errors, isValid } = validateLoginInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email })
    .then(user => {
      if (!user) {
        errors.email = 'User not found'
        return res.status(404).json(errors);
      }
      bcrypt.compare(password, user.password)
        .then(isMatch => {
          if (isMatch) {
            const payload = {
              id: user.id,
              name: user.name,
              email: user.email,
              phone: user.phone,
              role: user.role,
              avatar: user.avatar
            }
            jwt.sign(payload, 'secret', {
              expiresIn: 3600
            }, (err, token) => {
              if (err) console.error('There is some error in token', err);
              else {
                res.json({
                  success: true,
                  token: `Bearer ${token}`,
                  user: payload
                });
              }
            });
          }
          else {
            errors.password = 'Incorrect Password';
            return res.status(400).json(errors);
          }
        });
    });
});

router.get('/me', passport.authenticate('jwt', { session: false }), (req, res) => {
  return res.json({
    data: req.user
    // id: req.user.id,
    // name: req.user.name,
    // email: req.user.email
  });
});

//get user
router.post('/', async (req, res) => {
  let users, total = 0,
    page = req.body.page || 1, perPage = req.body.perPage || 10,
    search_text = req.body.search_text | ''
  try {
    total = await User.find({}, '-password');
    users = await User.find({}, '-password').sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever')
  }
  res.status(201).json({ data: users, length: users.length, total: total.length })
})

router.patch('/:id', async (req, res) => {
  const id = req.params.id
  let user;
  try {
    user = await User.findById(id)
  } catch (error) {
    res.status(500).json('Lỗi sever')
  }
  if (!user) {
    res.status(400).json({ user_id: 'Không tìm được môn!' })
  }
  if (!req.body.name) res.status(400).json({ name: 'Tên không được để trống' })
  user.role = req.body.role;
  user.phone = req.body.phone;
  user.name = req.body.name;
  try {
    await user.save()
  } catch (error) {
    res.status(500).json('Lỗi sever')
  }
  res.status(200).json({ data: user })
});

router.delete('/:id', async (req, res) => {
  const id = req.params.id
  let user;
  try {
    user = await User.findById(id)
  } catch (error) {
    res.status(500).json('Lỗi sever')
  }
  if (!user) {
    res.status(400).json({ user_id: 'Không tìm được user!' })
  }
  try {
    await user.deleteOne()
  } catch (error) {
    res.status(500).json('Lỗi sever')
  }
  res.status(200).json({ message: 'Xóa thành công' })
});
module.exports = router;