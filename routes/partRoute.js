const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const Parts = require('../models/partModel')
const Topics = require('../models/topicModel')

const validatePart = require('../validation/part');

//get all part
router.get('/', async (req, res) => {
  let parts;
  try {
    parts = await Parts.find().sort({ name: 1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: parts, total: parts.length })
})
// get part by page perPage
router.post('/', async (req, res) => {
  let parts, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Parts.find()
    parts = await Parts.find().populate('subject_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: parts, length: parts.length, total: total.length })
})
//get part by id
router.get('/part/:id', async (req, res) => {
  let part, id = req.params.id;
  try {
    part = await Parts.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json({ part_id: 'Không tìm được dạng!' })
  }
  res.status(201).json({ data: part })
})
// add part
router.post('/add', async (req, res) => {
  const { errors, isValid } = validatePart(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { name, subject_id, creator } = req.body
  const part = new Parts({
    name, subject_id,
    creator
  })
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Môn không tồn tại' })
  }
  try {
    await part.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Đã tạo dạng', data: part })
})
// delete part
router.delete('/:id', async (req, res) => {
  const id = req.params.id
  let part
  try {
    part = await Parts.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json({ part_id: 'Không tìm được dạng!' })
  }
  try {
    await part.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa dạng thành công!' })
})
// update part
router.patch('/:id', async (req, res) => {
  const id = req.params.id
  let part
  try {
    part = await Parts.findById(id).populate('subject_id')
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json({ subject_id: 'Không tìm được môn!' })
  }
  const { name, creator, subject_id } = req.body
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Môn không tồn tại' })
  }
  part.name = name;
  part.subject_id = subject_id;
  part.creator = creator
  try {
    await part.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Cập nhật môn học thành công', data: part })
})

//get part by subject_id
router.get('/subject/:id', async (req, res) => {
  const id = req.params.id;
  let parts
  try {
    parts = await Parts.find({ subject_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  let parts2 = []
  for (let part of parts) {
    let topics
    try {
      topics = await Topics.find({ part_id: part._id }).sort({ name: 1 })
      // console.log(topics)
    } catch (error) {
      res.status(500).json('Lỗi sever!')
    }
    part.topics = topics
    // console.log(part)

    // parts2.push(part)
  }
  res.status(200).json({ data: parts, total: parts.length })
})
// get part by subject_id with params
router.post('/subject/:id', async (req, res) => {
  const id = req.params.id
  let parts, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Parts.find({ subject_id: id })
    parts = await Parts.find({ subject_id: id }).populate('subject_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: parts, length: parts.length, total: total.length })
})
module.exports = router;