const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const Parts = require('../models/partModel')
const Topics = require('../models/topicModel')
const Questions = require('../models/questionModel')
const Exams = require('../models/examModel')
const Tests = require('../models/testModel')

//get all test 
router.get('/', async (req, res) => {
  let tests;
  try {
    tests = await Tests.find().sort({ data_start: -1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: tests, total: tests.length })
})
// get tests by page perPage
router.post('/', async (req, res) => {
  let tests, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Tests.find()
    tests = await Tests.find().populate('exam_id', 'subject_id').populate('user', 'name').populate('exam_id.subject_id', 'name')
      .sort({ date_start: -1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: tests, length: tests.length, total: total.length })
})
router.post('/me/:id', async (req, res) => {
  const id = req.params.id
  let tests, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Tests.find({ user: id })
    tests = await Tests.find({ user: id }).populate('exam_id', 'subject_id').populate('user', 'name').populate('exam_id.subject_id', 'name')
      .sort({ date_start: -1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: tests, length: tests.length, total: total.length })
})
//get test by id
router.get('/:id', async (req, res) => {
  let test, id = req.params.id;
  try {
    test = await Tests.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!test) {
    res.status(400).json({ test_id: 'Không tìm được bài thi!' })
  }
  res.status(201).json({ data: test })
})
// add test
router.post('/add', async (req, res) => {

  const { user, exam_id, date_start, answers } = req.body

  let users;
  try {
    users = await Users.findById(user)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!users) {
    res.status(400).json({ user_id: 'User không tồn tại' })
  }
  let exam;
  try {
    exam = await Exams.findById(exam_id).populate('ques.question_id')
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!exam) {
    res.status(400).json({ exam_id: 'Đề thi không tồn tại' })
  }
  let num_true = 0;
  exam.ques.map((ques, index) => {
    let answer_id;
    ques.question_id.answers.map(ans => {
      if (ans.isTrue === true) answer_id = ans._id
    })
    if (answers[index] == answer_id) num_true = num_true + 1
  })
  const mark = Math.round((10 / exam.ques.length) * num_true + "e+2") + "e-2"
  const newTest = new Tests({
    name: 'Bài làm',
    user,
    date_start,
    exam_id,
    answers,
    num_true,
    mark
  })
  try {
    await newTest.save()
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: newTest })
})
// delete test
router.delete('/:id', async (req, res) => {

  const id = req.params.id
  let test
  try {
    test = await Tests.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!test) {
    res.status(400).json({ test_id: 'Không tìm được đề thi!' })
  }
  try {
    await test.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa bài thi thành công!' })
})
// get ramdom exam
module.exports = router;