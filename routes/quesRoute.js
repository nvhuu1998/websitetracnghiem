const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const Parts = require('../models/partModel')
const Topics = require('../models/topicModel')
const Questions = require('../models/questionModel')
const validateQues = require('../validation/question');

//get all ques
router.get('/', async (req, res) => {
  let questions;
  try {
    questions = await Questions.find().sort({ name: 1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: questions, total: questions.length })
})
// get questions by page perPage
router.post('/', async (req, res) => {
  let questions, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Questions.find()
    questions = await Questions.find().populate('subject_id').populate('part_id').populate('topic_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: questions, length: questions.length, total: total.length })
})
//get question by id
router.get('/:id', async (req, res) => {
  let question, id = req.params.id;
  try {
    question = await Questions.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!question) {
    res.status(400).json({ question_id: 'Không tìm được câu hỏi!' })
  }
  res.status(201).json({ data: question })
})
// add question
router.post('/add', async (req, res) => {
  const { errors, isValid } = validateQues(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { name, subject_id, topic_id, part_id, image, question, answers, creator } = req.body
  const ques = new Questions({
    name, subject_id, topic_id, question, image, answers, part_id, creator
  })
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Môn không tồn tại' })
  }
  let part
  try {
    part = await Parts.findById(part_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json({ part_id: 'Dạng không tồn tại' })
  }
  let topic
  try {
    topic = await Topics.findById(topic_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!topic) {
    res.status(400).json({ topic_id: 'Chủ đề không tồn tại' })
  }
  try {
    await ques.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Đã tạo câu hỏi', data: ques })
})
// delete topic
router.delete('/:id', async (req, res) => {
  const id = req.params.id
  let ques
  try {
    ques = await Questions.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!ques) {
    res.status(400).json({ question_id: 'Không tìm được chủ đề!' })
  }
  try {
    await ques.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa câu hỏi thành công!' })
})
// update question
router.patch('/:id', async (req, res) => {
  const id = req.params.id
  let ques
  try {
    ques = await Questions.findById(id).populate('subject_id').populate('part_id').populate('topic_id')
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!ques) {
    res.status(400).json({ question_id: 'Không tìm được chủ đề!' })
  }
  const { name, creator, subject_id, part_id, topic_id, question, answers } = req.body
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json({ creator: 'creator không tồn tại' })
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Môn không tồn tại' })
  }
  let part
  try {
    part = await Parts.findById(part_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json({ part_id: 'Chủ đề không tồn tại' })
  }
  let topic
  try {
    topic = await Topics.findById(topic_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!topic) {
    res.status(400).json({ topic_id: 'Chủ đề không tồn tại' })
  }
  ques.name = name;
  ques.subject_id = subject_id;
  ques.part_id = part_id;
  ques.topic_id = topic_id;
  ques.question = question;
  ques.answers = answers;
  ques.creator = creator
  try {
    await ques.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Cập nhật chủ đề học thành công', data: ques })
})

//get question by subject_id
router.get('/subject/:id', async (req, res) => {
  const id = req.params.id;
  let ques
  try {
    ques = await Questions.find({ subject_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: ques, total: ques.length })
})
//get questions by subject_id with params
router.post('/subject/:id', async (req, res) => {
  const id = req.params.id
  let ques, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Questions.find({ subject_id: id })
    ques = await Questions.find({ subject_id: id }).populate('subject_id').populate('part_id').populate('topic_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: ques, length: ques.length, total: total.length })
})
//get question by part_id
router.get('/part/:id', async (req, res) => {
  const id = req.params.id;
  let ques
  try {
    ques = await Questions.find({ part_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: ques, total: ques.length })
})
//get questions by part with params
router.post('/part/:id', async (req, res) => {
  const id = req.params.id
  let ques, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Questions.find({ part_id: id })
    ques = await Questions.find({ part_id: id }).populate('subject_id').populate('part_id').populate('topic_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: ques, length: ques.length, total: total.length })
})
//get question by topic_id
router.get('/topic/:id', async (req, res) => {
  const id = req.params.id;
  let ques
  try {
    ques = await Questions.find({ topic_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: ques, total: ques.length })
})
//get questions by topic with params
router.post('/part/:id', async (req, res) => {
  const id = req.params.id
  let ques, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Questions.find({ topic_id: id })
    ques = await Questions.find({ topic_id: id }).populate('subject_id').populate('part_id').populate('topic_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: ques, length: ques.length, total: total.length })
})
module.exports = router;