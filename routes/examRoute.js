const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const Parts = require('../models/partModel')
const Topics = require('../models/topicModel')
const Questions = require('../models/questionModel')
const Exams = require('../models/examModel')
const validateSubject = require('../validation/subject');
// var random = require('mongoose-random');
//get all exam 
router.get('/', async (req, res) => {
  let exams;
  try {
    exams = await Exams.find().sort({ name: 1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: exams, total: exams.length })
})
// get exam by page perPage
router.post('/', async (req, res) => {
  let exams, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Exams.find()
    exams = await Exams.find().sort({ name: 1 }).populate('subject_id', 'name')
      .skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: exams, length: exams.length, total: total.length })
})
//get exam by id
router.get('/:id', async (req, res) => {
  let exam, id = req.params.id;
  try {
    exam = await Exams.findById(id).populate('ques.question_id').populate('subject_id', 'name')
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!exam) {
    res.status(400).json({ exam_id: 'Không tìm được đề thi!' })
  }
  res.status(201).json({ data: exam })
})
// add exam
router.post('/add', async (req, res) => {

  const { subject_id } = req.body
  let subject;
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Vui lòng chọn môn' })
  }
  Questions.findRandom({}, {}, { limit: 50 }, function (err, results) {
    if (!err) {
      let questions = []
      results.map(ques => {
        questions.push({ question_id: ques._id })
      })
      const newExam = new Exams({
        name: 'Đề thi thử',
        type: 1,
        subject_id: subject_id,
        ques: questions
      })
      newExam.save().then(exam => {
        res.status(201).json({ data: exam })
      })
    } else {
      res.status(500).json('Lỗi sever!')
    }
  });
})
router.post('/add/topic', async (req, res) => {

  const { subject_id, topics } = req.body
  console.log(req.body)
  let subject;
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json({ subject_id: 'Môn không tồn tại' })
  }
  Questions.findRandom({ topic_id: { $in: topics } }, {}, { limit: 50 }, function (err, results) {
    if (!err) {
      let questions = []
      console.log(results)
      if (results) {
        results.map(ques => {
          // question_id=ques._id
          questions.push({ question_id: ques._id })
        })
        const newExam = new Exams({
          name: 'Đề thi trộn',
          type: 2,
          subject_id: subject_id,
          ques: questions
        })
        newExam.save().then(exam => {
          res.status(201).json({ data: exam })
        })
      }
    } else {
      res.status(500).json('Lỗi sever!')
    }
  });
})
// delete subject
router.delete('/:id', async (req, res) => {

  const id = req.params.id
  let exam
  try {
    exam = await Exams.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!exam) {
    res.status(400).json({ exam_id: 'Không tìm được đề thi!' })
  }
  try {
    await exam.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa đề thi thành công!' })
})
// get ramdom exam
router.post('/random', async (req, res) => {
  const subject_id = req.body.subject_id
  Exams.findRandom({ type: 1, subject_id: subject_id }, {}, { limit: 1 }, function (err, results) {
    if (!err) {
      res.status(201).json({ data: results })
    } else {
      res.status(500).json('Lỗi sever!')
    }
  })
})
module.exports = router;