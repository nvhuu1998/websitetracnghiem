const express = require('express');
const router = express.Router();
const upload = require('../middleware/file-upload');
const Resize = require('../middleware/Resize');
const path = require('path');
const UploadModel = require('../models/uploadModel');

router.post('/', upload.single('image'), async (req, res) => {
  const imagePath = path.join(__dirname, '/public/images');
  const fileUpload = new Resize(imagePath);
  if (!req.file) {
    res.status(401).json({ image: 'Please provide an image' });
  }
  const filename = await fileUpload.save(req.file.buffer);
  return res.status(200).json({ name: filename });
})
module.exports = router;