const express = require('express');
const router = express.Router();
const Users = require('../models/userModel');
const Subjects = require('../models/subjectModel')
const Parts = require('../models/partModel')
const Topics = require('../models/topicModel')
const validateTopic = require('../validation/topic');

//get all topic
router.get('/', async (req, res) => {
  let topics;
  try {
    topics = await Topics.find().sort({ name: 1 })
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: topics, total: topics.length })
})
// get topic by page perPage
router.post('/', async (req, res) => {
  let topics, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Topics.find()
    topics = await Topics.find().populate('subject_id').populate('part_id').sort({ name: 1 })
      .skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: topics, length: topics.length, total: total.length })
})
//get topic by id
router.get('/:id', async (req, res) => {
  let topic, id = req.params.id;
  try {
    topic = await Topics.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!topic) {
    res.status(400).json('Không tìm được chủ đề!')
  }
  res.status(201).json({ data: topic })
})
// add topic
router.post('/add', async (req, res) => {
  const { errors, isValid } = validateTopic(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { name, subject_id, part_id, creator } = req.body
  const topic = new Topics({
    name, subject_id, part_id,
    creator
  })
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json('creator không tồn tại')
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json('Môn không tồn tại')
  }
  let part
  try {
    part = await Parts.findById(part_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json('Dạng không tồn tại')
  }
  try {
    await topic.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Đã tạo chủ đề', data: topic })
})
// delete topic
router.delete('/:id', async (req, res) => {
  const id = req.params.id
  let topic
  try {
    topic = await Topics.findById(id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!topic) {
    res.status(400).json('Không tìm được chủ đề!')
  }
  try {
    await topic.deleteOne()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ message: 'Xóa chủ đề thành công!' })
})
// update topic
router.patch('/:id', async (req, res) => {
  const id = req.params.id
  let topic
  try {
    topic = await Topics.findById(id).populate('subject_id').populate('part_id')
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!topic) {
    res.status(400).json('Không tìm được chủ đề!')
  }
  const { name, creator, subject_id, part_id } = req.body
  let user
  try {
    user = await Users.findById(creator)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!user) {
    res.status(400).json('creator không tồn tại')
  }
  let subject
  try {
    subject = await Subjects.findById(subject_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!subject) {
    res.status(400).json('Môn không tồn tại')
  }
  let part
  try {
    part = await Parts.findById(part_id)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  if (!part) {
    res.status(400).json('Chủ đề không tồn tại')
  }
  topic.name = name;
  topic.subject_id = subject_id;
  topic.part_id = part_id;
  topic.creator = creator
  try {
    await topic.save()
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ message: 'Cập nhật chủ đề học thành công', data: topic })
})

//get topic by subject_id
router.get('/subject/:id', async (req, res) => {
  const id = req.params.id;
  let topics
  try {
    topics = await Topics.find({ subject_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: topics, total: topics.length })
})
//get topics by subject_id with params
router.post('/subject/:id', async (req, res) => {
  const id = req.params.id
  let topics, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Topics.find({ subject_id: id })
    topics = await Topics.find({ subject_id: id }).populate('subject_id').populate('part_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: topics, length: topics.length, total: total.length })
})
//get topic by part_id
router.get('/part/:id', async (req, res) => {
  const id = req.params.id;
  let topics
  try {
    topics = await Topics.find({ part_id: id }).sort({ name: 1 })
  } catch (error) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(200).json({ data: topics, total: topics.length })
})
//get topics by part_id with params
router.post('/part/:id', async (req, res) => {
  const id = req.params.id
  let topics, total, page = req.body.page || 1, perPage = req.body.perPage || 10
  try {
    total = await Topics.find({ part_id: id })
    topics = await Topics.find({ part_id: id }).populate('subject_id').populate('part_id')
      .sort({ name: 1 }).skip((perPage * page) - perPage).limit(perPage)
  } catch (err) {
    res.status(500).json('Lỗi sever!')
  }
  res.status(201).json({ data: topics, length: topics.length, total: total.length })
})
module.exports = router;