const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const HttpError = require('../models/http-error');
const subjectModel = require('../models/subjectModel');
const User = require('../models/userModel');

const getSubjects = async (req, res, next) => {
    let subjects;
    try {
        subjects = await subjectModel.find();
    } catch (err) {
        const error = new HttpError(
            'Fetching subject failed, please try again later.',
            500
        );
        return next(error);
    }
    res.json({ subjects: subjects.map(subject => subject.toObject({ getters: true })), length: subjects.length });
};

const getSubjectById = async (req, res, next) => {
    const subjectId = req.params.pid;

    let subject;
    try {
        subject = await subjectModel.findById(subjectId);
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not find a subject.',
            500
        );
        return next(error);
    }

    if (!subject) {
        const error = new HttpError(
            'Could not find a subject for the provided id.',
            404
        );
        return next(error);
    }

    res.json({ subject: subject.toObject({ getters: true }) });
};


const createSubject = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(
            new HttpError('Invalid inputs passed, please check your data.', 422)
        );
    }

    const { name, creator } = req.body;

    const createdSubject = new subjectModel({
        name,
        creator,
        parts: []
    });

    let user;
    try {
        user = await User.findById(creator);
    } catch (err) {
        const error = new HttpError('Creating subject failed, please try again', 500);
        return next(error);
    }

    if (!user) {
        const error = new HttpError('Could not find user for provided id', 404);
        return next(error);
    }

    // console.log(user);

    try {
        await createdSubject.save();
    } catch (err) {
        const error = new HttpError(
            'Create Subject failed, please try again.',
            500
        );
        return next(error);
    }

    res.status(201).json({ subject: createdSubject });
};

const updateSubject = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(
            new HttpError('Invalid inputs passed, please check your data.', 422)
        );
    }

    const { name } = req.body;
    const subjectId = req.params.pid;

    let subject;
    try {
        subject = await subjectModel.findById(subjectId);
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not update place.',
            500
        );
        return next(error);
    }

    subject.name = name;
    try {
        await subject.save();
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not update subject.',
            500
        );
        return next(error);
    }

    res.status(200).json({ subject: subject.toObject({ getters: true }) });
};

const deleteSubject = async (req, res, next) => {
    const subjectId = req.params.pid;

    let subject;
    try {
        subject = await subjectModel.findById(subjectId).populate('creator');
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not delete subject.',
            500
        );
        return next(error);
    }

    if (!subject) {
        const error = new HttpError('Could not find subject for this id.', 404);
        return next(error);
    }
    try {
        await subject.deleteOne();
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not delete subject.',
            500
        );
        return next(error);
    }

    res.status(200).json({ message: 'Deleted subject.' });
};

exports.getSubjects = getSubjects;
exports.getSubjectById = getSubjectById;
exports.createSubject = createSubject;
exports.updateSubject = updateSubject;
exports.deleteSubject = deleteSubject;
