const { validationResult } = require('express-validator');

const HttpError = require('../models/http-error');
const User = require('../models/userModel');

const getUsers = async (req, res, next) => {
    console.log(req.body)
    let users;
    let search_text = req.body.search_text || '';
    let total = 0;
    let page = req.body.page ? req.body.page : 1;
    let perPage = req.body.perPage ? req.body.perPage : 10;
    try {
        total = await User.find({
            $or: [{ name: { $regex: search_text, $options: "i" } },
            { email: { $regex: search_text, $options: "i" } }]
        }, '-password');
        users = await User.find({
            $or: [{ name: { $regex: search_text, $options: "i" } },
            { email: { $regex: search_text, $options: "i" } }]
        }, '-password').skip((perPage * page) - perPage).limit(perPage)
    } catch (err) {
        const error = new HttpError(
            'Fetching users failed, please try again later.',
            500
        );
        return next(error);
    }
    res.json({ data: users.map((user, index) => user), length: users.length, total: total.length });
};

const signup = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(
            new HttpError('Invalid inputs passed, please check your data.', 422)
        );
    }
    const { name, email, password, phone, role } = req.body;

    let existingUser
    try {
        existingUser = await User.findOne({ email: email })
    } catch (err) {
        const error = new HttpError(
            'Signing up failed, please try again later.',
            500
        );
        return next(error);
    }

    if (existingUser) {
        const error = new HttpError(
            'User exists already, please login instead.',
            422
        );
        return next(error);
    }

    const createdUser = new User({
        name,
        email,
        password,
        phone,
        role
    });

    try {
        await createdUser.save();
    } catch (err) {
        const error = new HttpError(
            'Signing up failed, please try again.',
            500
        );
        return next(error);
    }

    res.status(201).json({ user: createdUser.toObject({ getters: true }) });
};

const login = async (req, res, next) => {
    const { email, password } = req.body;

    let existingUser;

    try {
        existingUser = await User.findOne({ email: email })
    } catch (err) {
        const error = new HttpError(
            'Logging in failed, please try again later.',
            500
        );
        return next(error);
    }

    if (!existingUser || existingUser.password !== password) {
        const error = new HttpError(
            'Invalid credentials, could not log you in.',
            401
        );
        return next(error);
    }

    res.json({ message: 'Logged in!' });
};

exports.getUsers = getUsers;
exports.signup = signup;
exports.login = login;
