const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const HttpError = require('../models/http-error');
const partModel = require('../models/partModel');
const subjectModel = require('../models/subjectModel');
const User = require('../models/userModel');

const getParts = async (req, res, next) => {
    let parts;
    try {
        parts = await partModel.find();
    } catch (err) {
        const error = new HttpError(
            'Fetching part failed, please try again later.',
            500
        );
        return next(error);
    }
    res.json({ parts: parts.map(part => part.toObject({ getters: true })), length: parts.length });
};

const getPartById = async (req, res, next) => {
    const partId = req.params.pid;

    let part;
    try {
        part = await partModel.findById(partId).populate('subject_id');
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not find a part.',
            500
        );
        return next(error);
    }

    if (!part) {
        const error = new HttpError(
            'Could not find a part for the provided id.',
            404
        );
        return next(error);
    }

    res.json({ part: part.toObject({ getters: true }) });
};

const getPartBySubjectId = async (req, res, next) => {
    const subjectId = req.params.pid;

    let subjectWithPart;
    try {
        subjectWithPart = await subjectModel.findById(subjectId).populate('parts');
    } catch (err) {
        const error = new HttpError(
            'Fetching subject failed, please try again later',
            500
        );
        return next(error);
    }

    if (!subjectWithPart || subjectWithPart.parts.length === 0) {
        return next(
            new HttpError('Could not find part for the subject id.', 404)
        );
    }

    res.json({
        parts: subjectWithPart.parts.map(part =>
            part.toObject({ getters: true })
        )
    });
};


const createPart = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(
            new HttpError('Invalid inputs passed, please check your data.', 422)
        );
    }

    const { name, subject_id, creator } = req.body;

    const createdPart = new partModel({
        name,
        subject_id,
        creator
    });

    let user;
    try {
        user = await User.findById(creator);
    } catch (err) {
        const error = new HttpError('Creating part failed, please try again', 500);
        return next(error);
    }

    if (!user) {
        const error = new HttpError('Could not find user for provided id', 404);
        return next(error);
    }
    let subject;
    try {
        subject = await subjectModel.findById(subject_id);
    } catch (err) {
        const error = new HttpError('Creating part failed, please try again', 500);
        return next(error);
    }

    if (!subject) {
        const error = new HttpError('Could not find subject for subject id', 404);
        return next(error);
    }

    console.log(subject);

    try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        await createdPart.save({ session: sess });
        subject.parts.push(createdPart);
        await subject.save({ session: sess });
        await sess.commitTransaction();
    } catch (err) {
        const error = new HttpError(
            'Creating place failed3, please try again.',
            500
        );
        console.log(err)
        return next(error);
    }


    res.status(201).json({ part: createdPart });
};

const updatePart = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(
            new HttpError('Invalid inputs passed, please check your data.', 422)
        );
    }

    const { name } = req.body;
    const partId = req.params.pid;

    let part;
    try {
        part = await partModel.findById(partId);
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not update place.',
            500
        );
        return next(error);
    }

    part.name = name;
    try {
        await part.save();
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not update part.',
            500
        );
        return next(error);
    }

    res.status(200).json({ part: part.toObject({ getters: true }) });
};

const deletePart = async (req, res, next) => {
    const partId = req.params.pid;

    let part;
    try {
        part = await partModel.findById(partId).populate('subject_id');
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not delete part.',
            500
        );
        return next(error);
    }

    if (!part) {
        const error = new HttpError('Could not find part for this id.', 404);
        return next(error);
    }
    console.log(part)
    try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        await part.remove({ session: sess });
        part.subject_id.parts.pull(part);
        await part.subject_id.save({ session: sess });
        await sess.commitTransaction();
    } catch (err) {
        const error = new HttpError(
            'Something went wrong, could not delete part.!',
            500
        );
        return next(error);
    }
    res.status(200).json({ message: 'Deleted part.' });
};

exports.getParts = getParts;
exports.getPartById = getPartById;
exports.getPartBySubjectId = getPartBySubjectId;
exports.createPart = createPart;
exports.updatePart = updatePart;
exports.deletePart = deletePart;
