const mongoose = require("mongoose");
const uploadSchema = new mongoose.Schema({
    img: { type: String, required: true }
})
module.exports = mongoose.model('uploads', uploadSchema);