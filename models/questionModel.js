const mongoose = require('mongoose');
var random = require('mongoose-simple-random');
const questionSchema = new mongoose.Schema({
  name: { type: String, required: true },
  question: { type: String, required: true },
  image: { type: String },
  subject_id: { type: mongoose.Types.ObjectId, required: true, ref: 'subject' },
  part_id: { type: mongoose.Types.ObjectId, required: true, ref: 'parts' },
  topic_id: { type: mongoose.Types.ObjectId, required: true, ref: 'topics' },
  answers: [{
    ans: { type: String, required: true },
    isTrue: { type: Boolean, required: true, default: false }
  }],
  creator: { type: mongoose.Types.ObjectId, required: true, ref: 'users' },
  date_create: { type: Date, required: true, default: Date.now() }
});
questionSchema.plugin(random);
module.exports = mongoose.model('questions', questionSchema);