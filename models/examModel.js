var mongoose = require('mongoose');
var random = require('mongoose-simple-random');
const examSchema = new mongoose.Schema({
  name: { type: String, required: true },
  subject_id: { type: mongoose.Types.ObjectId, ref: 'subject' },
  type: { type: Number, required: true },
  date_create: { type: Date, required: true, default: Date.now() },
  ques: [{
    question_id: { type: mongoose.Types.ObjectId, ref: 'questions' },
  }],
});
examSchema.plugin(random);
module.exports = mongoose.model('exam', examSchema);