const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  phone: { type: String },
  role: { type: Number, required: true, default: 2 },
  avatar: { type: String },
  date_create: { type: Date, default: Date.now }
});
module.exports = mongoose.model('users', userSchema);