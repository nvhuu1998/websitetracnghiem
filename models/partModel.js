const mongoose = require('mongoose');
const partSchema = new mongoose.Schema({
    name: { type: String, required: true },
    subject_id: { type: mongoose.Types.ObjectId, required: true, ref: 'subject' },
    creator: { type: mongoose.Types.ObjectId, required: true, ref: 'users' },
    date_create: { type: Date, required: true, default: Date.now() },
    topics: [{
        id: { type: mongoose.Types.ObjectId, required: true },
        name: { type: String, required: true }
    }]
});
module.exports = mongoose.model('parts', partSchema);