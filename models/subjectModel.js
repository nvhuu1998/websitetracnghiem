const mongoose = require('mongoose');
const subjectSchema = new mongoose.Schema({
    name: { type: String, required: true },
    creator: { type: mongoose.Types.ObjectId, required: true, ref: 'users' },
    countParts: { type: Number, required: true, default: 0 },
    date_create: { type: Date, required: true, default: Date.now() }
});
module.exports = mongoose.model('subject', subjectSchema);