var mongoose = require('mongoose');
const testSchema = new mongoose.Schema({
  name: { type: String },
  user: { type: mongoose.Types.ObjectId, required: true, ref: 'users' },
  date_start: { type: Date, required: true },
  exam_id: { type: mongoose.Types.ObjectId, required: true, ref: 'exam' },
  answers: [{ type: mongoose.Types.ObjectId, required: true }],
  num_true: { type: Number, default: 0 },
  mark: { type: Number, default: 0 },
  date_end: { type: Date, required: true, default: Date.now() },
});
module.exports = mongoose.model('tests', testSchema);