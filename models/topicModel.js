const mongoose = require('mongoose');
const topicShema = new mongoose.Schema({
  name: { type: String, required: true },
  subject_id: { type: mongoose.Types.ObjectId, required: true, ref: 'subject' },
  part_id: { type: mongoose.Types.ObjectId, required: true, ref: 'parts' },
  creator: { type: mongoose.Types.ObjectId, required: true, ref: 'users' },
  date_create: { type: Date, required: true, default: Date.now() }
});
module.exports = mongoose.model('topics', topicShema);