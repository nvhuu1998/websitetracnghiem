const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateRegisterInput(data) {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.phone = !isEmpty(data.phone) ? data.phone : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password_confirm = !isEmpty(data.password_confirm) ? data.password_confirm : '';

    if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
        errors.name = 'Tên phải có ít nhất 2 ký tự';
    }

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Tên không được để trống';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email không đúng';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email không được để trống';
    }
    if (Validator.isEmpty(data.phone)) {
        errors.phone = 'Phone không được để trống';
    }
    if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
        errors.password = 'Mật khẩu phải có ít nhất 6 ký tự';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Mật khẩu không được để trống';
    }

    if (!Validator.isLength(data.password_confirm, { min: 6, max: 30 })) {
        errors.password_confirm = 'Mật khẩu phải có ít nhất 6 ký tự';
    }

    if (!Validator.equals(data.password, data.password_confirm)) {
        errors.password_confirm = 'Không khớp mật khẩu';
    }

    if (Validator.isEmpty(data.password_confirm)) {
        errors.password_confirm = 'Mật khẩu không được để trống';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}