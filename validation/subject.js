const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateSubject(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : '';

  data.creator = !isEmpty(data.creator) ? data.creator : '';


  if (Validator.isEmpty(data.name)) {
    errors.name = 'name is required';
  }
  if (Validator.isEmpty(data.creator)) {
    errors.creator = 'creator is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
