const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateQues(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : '';
  data.question = !isEmpty(data.question) ? data.question : '';
  data.subject_id = !isEmpty(data.subject_id) ? data.subject_id : '';
  data.part_id = !isEmpty(data.part_id) ? data.part_id : '';
  data.topic_id = !isEmpty(data.topic_id) ? data.topic_id : '';
  data.creator = !isEmpty(data.creator) ? data.creator : '';

  if (Validator.isEmpty(data.name)) {
    errors.name = 'Tê câu hỏi không được trống';
  }
  if (Validator.isEmpty(data.question)) {
    errors.question = 'Câu hỏi không được trống';
  }
  if (Validator.isEmpty(data.subject_id)) {
    errors.subject_id = 'Môn học không được trống';
  }
  if (Validator.isEmpty(data.part_id)) {
    errors.part_id = 'Dạng không được trống';
  }
  if (Validator.isEmpty(data.topic_id)) {
    errors.topic_id = 'Chủ đề không được trống';
  }
  if (data.answers.length === 0) {
    errors.answers = 'Phải có ít nhất 1 đáp án'
  }
  data.answers.map((ans, index) => {
    if (Validator.isEmpty(ans.ans)) {
      errors.answers[index] = 'Đáp án ko được trống'
    }
  })
  if (Validator.isEmpty(data.creator)) {
    errors.creator = 'creator is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
