const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const userRoute = require('./routes/userRoute');
const subjectRoute = require('./routes/subjectRoute');
const partRoute = require('./routes/partRoute');
const topicRoute = require('./routes/topicRoute');
const uploadRoute = require('./routes/uploadRoute');
const quesRoute = require('./routes/quesRoute');
const examRoute = require('./routes/examRoute');
const testRoute = require('./routes/testRoute');

// const users = require('./routes/users');
const passport = require('passport');
const HttpError = require('./models/http-error');
const path = require('path');

const url = 'mongodb+srv://nvhuu:huu270816@cluster0.37o7z.mongodb.net/nvhuu?retryWrites=true&w=majority';
const app = express();

app.use(passport.initialize());
require('./passport')(passport);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
    next();
});

app.use('/public', express.static('public'));
app.use('/api/users', userRoute);
// app.use('/api/users', users);
app.use('/api/subject', subjectRoute);
app.use('/api/part', partRoute);
app.use('/api/topic', topicRoute);
app.use('/api/ques', quesRoute);
app.use('/api/img', uploadRoute);
app.use('/api/exam', examRoute);
app.use('/api/test', testRoute);
app.use((req, res, next) => {
    const error = new HttpError('Could not find this route.', 404);
    throw error;
});

// app.use((error, req, res, next) => {
//     if (res.headerSent) {
//         return next(error);
//     }
//     res.status(error.code || 500);
//     res.json({ message: error.message || 'An unknown error occurred!' });
// });


mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
    .then(() => {
        app.listen(5000)
        console.log('connected db');
    })
    .catch(error => {
        console.log(error);
    });